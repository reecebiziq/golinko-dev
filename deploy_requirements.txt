# Requirements for server deployment
-r requirements.txt
psycopg2
pylibmc==1.2.3
greenlet==0.4.0
gevent==0.13.7
gunicorn==0.14.6
