#!/bin/sh
# Backup database to s3
# Uses the s3cmd utility to do the upload

DATE=`date +"%Y%m%d%H%M%S"`
DB=golinkodesign
DUMPFILE=/tmp/"$DATE"."$DB".sql.gz
BUCKET=golinko-backup
pg_dump $DB | gzip > $DUMPFILE
s3cmd --no-progress put $DUMPFILE s3://"$BUCKET"/db/
rm $DUMPFILE
exit 0;
