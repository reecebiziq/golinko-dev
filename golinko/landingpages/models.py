from django.db import models

from cms.apps.utils.fields import AutoSlugField
from cms.apps.media.models import RelatedImagesField

from golinko.sitepages.models import SitePage


class LandingPage(models.Model):
    title = models.CharField(max_length=50)
    slug = AutoSlugField(max_length=50, unique=True,
                         prepopulate_from='title')
    headline = models.CharField(max_length=255)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    active = models.BooleanField(default=True)

    content1_title = models.CharField('Title 1', max_length=255)
    content1_text = models.TextField('Text 1')
    content2_title = models.CharField('Title 2', max_length=255)
    content2_text = models.TextField('Text 2')
    link = models.CharField(
        max_length=255, null=True, blank=True,
        help_text='Link must be such as, for example: <b>/p/sitepage/</b>'
        '- if the link is to sitepage of this site, <b>/l/landingpage/</b>'
        '- if the link is to landingpage of this site,'
        ' or <b>http://site.com</b> - if the link is to another site')
    link_text = models.CharField(max_length=255, blank=True)

    content3_title = models.CharField('Col 1 Title', max_length=255)
    content3_text = models.TextField('Col 1 Text')
    content3_image = models.ForeignKey('media.Image',
                                       verbose_name='Col 1 Image',
                                       null=True, blank=True)
    content4_title = models.CharField('Col 2 Title', max_length=255)
    content4_text = models.TextField('Col 2 Text')

    column_layout = models.CharField(max_length=10,
                                     default='3col',
                                     choices=(
                                         ('3col', '3 column'),
                                         ('2col', '2 column'),
                                         ('1col', '1 column')))
    column1_title = models.CharField('Col 1 Title', max_length=255)
    column1_text = models.TextField('Col 1 Text')
    column2_title = models.CharField('Col 2 Title', max_length=255)
    column2_text = models.TextField('Col 2 Text')
    column3_title = models.CharField('Col 3 Title', max_length=255)
    column3_text = models.TextField('Col 3 Text')

    show_contact_form = models.BooleanField(default=False,
            help_text="Show a contact form on this page?")

    show_in_sitemap = models.BooleanField(default=True)
    sitemap_title = models.CharField(max_length=255, blank=True)

    images = RelatedImagesField()

    class Meta:
        verbose_name = "SEO Landing page"
        verbose_name_plural = "SEO Landing pages"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'landing_page', (self.slug,)


class PassthroughPage(models.Model):
    title = models.CharField(max_length=50)
    meta_keywords = models.TextField(blank=True)
    meta_description = models.TextField(blank=True)
    active = models.BooleanField(default=True)
    image = models.ForeignKey('media.Image')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = "Splash page"
        verbose_name_plural = "Splash pages"


class PassthroughOverlay(models.Model):
    passthrough_page = models.ForeignKey(PassthroughPage,
                                         related_name='overlays')

    link = models.CharField(
        max_length=255, null=True, blank=True,
        help_text='Link must be such as, for example: <b>/p/sitepage/</b>'
        '- if the link is to sitepage of this site, <b>/l/landingpage/</b>'
        '- if the link is to landingpage of this site,'
        ' or <b>http://site.com</b> - if the link is to another site')
    image = models.ForeignKey('media.Image',
                              help_text='Hover image')
    width = models.PositiveIntegerField(default=100)
    height = models.PositiveIntegerField(default=100)
    top = models.PositiveIntegerField(default=0)
    left = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Image Overlay'

    def __unicode__(self):
        return ''
