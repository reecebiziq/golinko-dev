from django.contrib import admin
from django.db import models

from cms.apps.utils.forms.widgets import VisualEditor
from cms.apps.media.admin.widgets import ImageForeignKeyWidget
from cms.apps.media.admin.options import RelatedImagesInline

from .models import (LandingPage, PassthroughPage, PassthroughOverlay)


class LandingPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'active',)
    prepopulated_fields = {'slug': ('title',)}
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'headline', 'meta_keywords',
                       'meta_description', 'active')
        }),
        ('Main Content', {
            'fields': ('content1_title', 'content1_text', 'content2_title',
                       'content2_text', 'link', 'link_text')
        }),
        ('Second Row', {
            'fields': ('content3_title', 'content3_text', 'content3_image',
                       'show_contact_form',
                       'content4_title', 'content4_text')
        }),
        ('Third Row', {
            'fields': ('column_layout', 'column1_title', 'column1_text',
                       'column2_title', 'column2_text', 'column3_title',
                       'column3_text')
        }),
        ('Site Map', {
            'fields': ('show_in_sitemap', 'sitemap_title',)
        }),
    )
    raw_id_fields = ('content3_image',)
    inlines = [RelatedImagesInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content3_image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        elif (isinstance(db_field, models.TextField) and
                not db_field.name.startswith('meta')):
            kwargs['widget'] = VisualEditor
        return super(LandingPageAdmin, self).formfield_for_dbfield(db_field,
                                                                   **kwargs)


class PassthroughOverlayInline(admin.StackedInline):
    model = PassthroughOverlay
    extra = 0
    template = 'admin/edit_inline/passthrough_overlay_inline.html'
    classes = ('collapsed-inlines',)
    raw_id_fields = ('image',)
    fields = ('link', 'image', ('width', 'height'), ('top', 'left'))

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(db_field.rel,
                                                     self.admin_site)
            return db_field.formfield(**kwargs)
        return super(PassthroughOverlayInline, self).formfield_for_dbfield(
            db_field, **kwargs)


class PassthroughPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'active',)
    raw_id_fields = ('image',)
    inlines = [PassthroughOverlayInline]

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        return super(PassthroughPageAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


admin.site.register(LandingPage, LandingPageAdmin)
admin.site.register(PassthroughPage, PassthroughPageAdmin)
