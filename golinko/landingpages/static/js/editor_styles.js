CKEDITOR.addStylesSet('golinko_styles',
[
   // Block Styles
   {name: 'h2:standout', element:'h2', attributes: {'class': 'standout'}},
   {name: 'h3:standout', element:'h3', attributes: {'class': 'standout'}},

   // Inline Styless
   {name: 'small', element: 'span', attributes: {'class': 'small'}},
   {name: 'quiet', element: 'span', attributes: {'class': 'quiet'}},
   {name: 'standout', element: 'span', attributes: {'class': 'standout'}},
    {name: 'fancy', element: 'span', attributes: {'class': 'fancy'}},
    {name: 'red-arrow', element: 'span', attributes: {'class': 'red-arrow'}},
    {name: 'black-arrow', element: 'span', attributes: {'class': 'black-arrow'}}
]);
