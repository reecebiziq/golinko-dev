from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('golinko.landingpages.views',
    url(r'^(?P<slug>[\w_-]+)/$', 'landing_page', name='landing_page'),
)
