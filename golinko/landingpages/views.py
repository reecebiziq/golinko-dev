from django import http
from django.views.generic import DetailView, TemplateView

from golinko.sitepages.forms import ContactForm
from .models import LandingPage, PassthroughPage


class LandingPageView(DetailView):
    queryset = LandingPage.objects.filter(active=True)
    template_name = 'landingpages/landing.html'
    context_object_name = 'page'
    form_class = ContactForm

    def get_context_data(self, **kwargs):
        context = super(LandingPageView, self).get_context_data(**kwargs)

        page = context['page']
        if getattr(page, 'show_contact_form', False):
            contact_form = ContactForm(self.request.POST or None)
            if self.request.method == 'POST':
                if contact_form.is_valid():
                    contact_form.send_mail()
                    return http.HttpResponseRedirect(
                            '{0}#contact-form'.format(self.request.path))
        else:
            contact_form = None

        context.update({
            'contact_form': contact_form,
        })

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if isinstance(context, http.HttpResponse):
            return context
        return self.render_to_response(context)

landing_page = LandingPageView.as_view()


class PassthroughView(DetailView):
    queryset = PassthroughPage.objects.filter(active=True)
    template_name = 'landingpages/pass_through.html'
    context_object_name = 'page'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            return queryset[0]
        except IndexError:
            raise http.Http404

passthrough = PassthroughView.as_view()


class SiteMapView(TemplateView):
    template_name = 'site_map.html'

    def get_context_data(self, **kwargs):
        context = super(SiteMapView, self).get_context_data(**kwargs)

        landing_pages = LandingPage.objects.filter(active=True,
                                                   show_in_sitemap=True)
        context.update({
            'landing_pages': landing_pages,
        })
        return context

site_map = SiteMapView.as_view()
