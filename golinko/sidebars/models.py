from django.db import models

from cms.apps.media.models import RelatedImagesField
from adminsortable.models import Sortable


class Award(Sortable):
    class Meta(Sortable.Meta):
        pass

    class Meta:
        verbose_name = "Honor/Award"
        verbose_name_plural = "Honors & Awards"

    title = models.CharField(max_length=100)
    text = models.TextField()
    published = models.BooleanField(default=True)

    def __unicode__(self):
        return self.title


class Testimonial(Sortable):
    class Meta(Sortable.Meta):
        pass

    quote = models.TextField()
    person = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    published = models.BooleanField(default=True)

    def __unicode__(self):
        res = ''
        if self.person:
            res += self.person[:30]
        res += ' - '
        if self.company:
            res += self.company[:30]
        return res


class AwardImage(models.Model):
    award_title_image = models.ForeignKey(
        'media.Image', related_name='award_title_image')
    category = models.OneToOneField(
        'menus.Category', verbose_name='Category',
        help_text='For correct view this '
                  'image, category should be: <b>about</b>')

    class Meta:
        verbose_name = "Honor/Award image"
        verbose_name_plural = "Honors & Awards images"

    def __unicode__(self):
        return self.award_title_image.filename.url
