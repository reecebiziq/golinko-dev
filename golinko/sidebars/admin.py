from django.contrib import admin
from django.db import models

from cms.apps.utils.forms.widgets import VisualEditor
from cms.apps.media.admin.widgets import ImageForeignKeyWidget
from .models import Award, Testimonial, AwardImage

from adminsortable.admin import SortableAdmin


class CustomSortableAdmin(SortableAdmin):
    # Assume that we always have such fields
    list_display = ('__unicode__', 'published',)


class CustomSortableCKAdmin(CustomSortableAdmin):
    """
    Custom class that change default widget at
    all models.TextField fields to VisualEditor.
    Also it brings Sortable functionality
    """

    def formfield_for_dbfield(self, db_field, **kwargs):
        if (isinstance(db_field, models.TextField) and
                not db_field.name.startswith('meta')):
            kwargs['widget'] = VisualEditor
        return super(CustomSortableCKAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


class AwardImageAdmin(admin.ModelAdmin):

    def formfield_for_dbfield(self, db_field, **kwargs):
        if 'image' in db_field.name:
            kwargs.pop('request')
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.rel, admin_site=self.admin_site)
            return db_field.formfield(**kwargs)
        return super(AwardImageAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)


admin.site.register(Award, CustomSortableCKAdmin)
admin.site.register(Testimonial, CustomSortableCKAdmin)
admin.site.register(AwardImage, AwardImageAdmin)
