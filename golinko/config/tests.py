import os
from .dev import *

config_path = os.path.join(BASE_PATH, 'config', 'local.py')
try:
    execfile(os.path.abspath(config_path))
except IOError:
    pass

DEBUG = False
TEMPLATE_DEBUG = DEBUG
TESTING = True
SKIP_SOUTH_TESTS = True

# Database Settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        #'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': ':memory:',                # Or path to database file if using sqlite3.
        'USER': '',            # Not used with sqlite3.
        'PASSWORD': '',    # Not used with sqlite3.
        'HOST': '',            # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',            # Set to empty string for default. Not used with sqlite3.
    }
}
#SPATIALITE_SQL = os.path.join(BASE_PATH, 'config', 'init_spatialite-3.0.sql')

# Use the following for postgis
#TEST_RUNNER='django.contrib.gis.tests.run_tests'

# Override media path
MEDIA_ROOT = os.path.join(TEMP_DIR, 'media')
MEDIA_URL = '/m/'

INSTALLED_APPS = list(INSTALLED_APPS)
INSTALLED_APPS.remove('south')

if not DEBUG:
    STATIC_ROOT = os.path.join(BASE_PATH, 'static')

# Cache Setup (use locmem for tests)
CACHE_TIMEOUT = 60*5
CACHE_PREFIX = 'CMS'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'golinko-tests',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}


# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(TEMP_DIR, 'emails')

# Speed up tests by setting faster hash algo
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

# Logging for tests
LOGGING['loggers'].update({
    'cms': {
        'handlers': ['logfile'],
        'level': 'WARN',
        'propagate': True,
    },
})
