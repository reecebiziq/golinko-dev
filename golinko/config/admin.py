"""
Custom configuration for which models to display in admin index pages
"""
from django.contrib import admin

from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext as _

from cms.apps.cmsadmin.sites import ArtcodeAdminSite, AdminApp

# Override the global admin.site
admin.site = ArtcodeAdminSite()


# Custom admin app configuration
class AuthAdminApp(AdminApp):
    app = 'auth'
    verbose_name = 'User Management'
    models = ['User', 'Group']


class MediaAdminApp(AdminApp):
    app = 'media'
    verbose_name = 'Media Library'
    models = ['image', 'video', 'file']


# These apps will be hidden from the admin index pages.
hidden_apps = ['sites', 'taggit']
for app_label in hidden_apps:
    type('%sAdminApp' % app_label.title, (AdminApp,), {
        'app': app_label,
        'hidden': True,
    })


# Customize auth admin layout.
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name',
                    'group_col', 'is_staff', 'is_superuser', 'is_active',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_staff', 'is_active', 'is_superuser', 'user_permissions'), 'classes': ('collapse-closed',)}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined'), 'classes': ('collapse-closed',)}),
        (_('Groups'), {'fields': ('groups',)}),
    )
    #inlines = (RegistrationAdmin,)

    def group_col(self, obj):
        return ', '.join([g.name for g in obj.groups.all()])
    group_col.short_description = 'Groups'

admin.site.register(User, CustomUserAdmin)
admin.site.register(Group, GroupAdmin)


# Automatically discover apps that have admin modules
# but have not yet been registered.
admin.autodiscover()
