"""
Default settings for golinko
"""
import os
import cms

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Eric Bartels', 'ebartels@gmail.com'),
)
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = '<nocontact@golinkodesign.com>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[golinkodesign.com]'

# Root project dir
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
CMS_DIR = os.path.dirname(cms.__file__)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Dynamic Media
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_PATH), '..', 'htdocs', 'media')
MEDIA_URL = '/m/'

# Static Media
#STATIC_ROOT = os.path.join(os.path.dirname(BASE_PATH), 'staticfiles')
STATIC_URL = '/s/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_PATH, "static"),
)

## List of finder classes that know how to find static files in
## various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

# Django compressor
COMPRESS_OUTPUT_DIR = '_C'
COMPRESS_CSS_FILTERS = (
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
)
COMPRESS_JS_FILTERS = (
    'compressor.filters.jsmin.JSMinFilter',
    #'compressor.filters.jsmin.SlimItFilter',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "cms.apps.menus.context_processors.menuitems",
    "cms.apps.menus.context_processors.current_menuitem",
)

MIDDLEWARE_CLASSES = (
    #'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.doc.XViewMiddleware',
    #'django.middleware.transaction.TransactionMiddleware',
    #'cms.apps.utils.middleware.http.ProxyHeaderMiddleware',
    #'cms.apps.utils.threadlocal.middleware.ThreadLocalRequestMiddleware',
    #'cms.apps.utils.middleware.http.ProxyHeaderMiddleware',
    #'cms.apps.utils.middleware.ssl.SSLRedirect',
    'cms.apps.utils.middleware.AJAXSimpleExceptionResponse',
    #'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'golinko.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'golinko.wsgi.application'

# Secure URLS, using SSLRedirect middleware
SSL_URLS = (
    #'/admin/',
    #'/account/',
)

# Headers used by http proxy
#PROXY_HOST_HEADER = 'HTTP_X_FORWARDED_FOR'
#PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_PATH, 'templates'),
    os.path.join(CMS_DIR, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'cms.apps.cmsadmin',
    'django.contrib.admin',
    #'django.contrib.gis',
    #'django.contrib.admindocs',
    #'django.contrib.webdesign',
    #'django.contrib.humanize',
    'south',
    'keyedcache',
    'livesettings',
    'mptt',
    'storages',
    'sorl.thumbnail',
    'taggit',
    'taggit_templatetags',
    'compressor',
    'cms.apps.utils',
    'cms.apps.menus',
    'cms.apps.media',
    'golinko.landingpages',
    'adminsortable',
    'golinko.sitepages',
    'golinko.sidebars',
    'activelink',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'regular': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout', # ipython won't show stderr
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'cms': {
            'handlers': ['null'],
            'level': 'WARN',
            'propagate': True,
        },
    }
}

# Use cooke storage & fall back to session for larger messages
MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

# Default File Storage
DEFAULT_FILE_STORAGE = 'cms.apps.utils.storage.OverwriteFileSystemStorage'
FILE_UPLOAD_PERMISSIONS = 0664

# Auth & Profiles
AUTHENTICATION_BACKENDS = (
    #'cms.apps.utils.auth.backends.EmailAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = '/account/login/'
LOGIN_REDIRECT_URL = '/'

# Temp folders
TEMP_DIR = os.path.join(BASE_PATH, '..', 'tmp')
if not os.path.exists(TEMP_DIR):
    os.mkdir(TEMP_DIR)
FILE_UPLOAD_TEMP_DIR = os.path.join(TEMP_DIR)

# Thumbnail settings
THUMBNAIL_PREFIX = 'thumbcache/'
THUMBNAIL_ENGINE = 'cms.apps.media.thumbnail.PILEngine'
#THUMBNAIL_UPSCALE = False

# Widget to use as the VisualEditor widget
VISUAL_EDITOR = 'cms.apps.utils.forms.widgets.CKTextEditor'

USE_MENU_INDEX_PAGES = False

# Default Markup Filter for cms.apps.utils.markup
MARKUP_FILTERS = (
    ('smartypants', None),
)

# Admin editor
CK_EDITOR_CONFIG = {
    'width': '850px',
    'toolbar': [ ['Bold', 'Italic', 'Strike', '-', 'BulletedList',
            'NumberedList','Outdent', 'Indent', 'Blockquote'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
        ['Format', 'FontSize', 'Styles'],
        ['Link', 'Unlink', 'Image'],
        ['PasteFromWord', 'RemoveFormat', '-', 'Maximize', '-', 'Source']
    ],

    'contentsCss': STATIC_URL + 'css/editor_content.css',

    'filebrowserImageBrowseUrl': '/admin/media/image/file_browser/',
    'filebrowserBrowseUrl': '/admin/media/link_browser/',

    'stylesCombo_stylesSet': 'golinko_styles:' + STATIC_URL + 'js/editor_styles.js'
}


SOUTH_TESTS_MIGRATE  = False
