"""
Settings for use in development environment.
"""
from .defaults import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

# Database Settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'db.sqlite',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    },
}


INTERNAL_IPS = ('127.0.0.1', '10.0.2.2',)

# Cache Setup
CACHE_TIMEOUT = 60 * 10
CACHE_PREFIX = 'GOLINKO'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'golinko-cache',
        'KEY_PREFIX': CACHE_PREFIX,
        'TIMEOUT': CACHE_TIMEOUT
    }
}
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"
CACHE_MIDDLEWARE_SECONDS = CACHE_TIMEOUT
CACHE_MIDDLEWARE_KEY_PREFIX = CACHE_PREFIX
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True


# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(TEMP_DIR, 'emails')
DEFAULT_FROM_EMAIL = '<ebartels@gmail.com>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL

LOGGING['handlers'].update({
    'logfile': {
        'level': 'DEBUG',
        'formatter': 'regular',
        'class': 'logging.handlers.RotatingFileHandler',
        'filename': os.path.join(TEMP_DIR, 'django.log'),
        'maxBytes': '16777216', # 16megabytes
    },
})
LOGGING['loggers'].update({
    'cms': {
        'handlers': ['console', 'logfile'],
        'level': 'DEBUG',
        'propagate': True,
    },
})

# Django compressor
COMPRESS_PRECOMPILERS = (
    # LESS files (note: make sure lessc is installed and on the your PATH)
    ('text/less', 'lessc {infile} {outfile}'),
)
COMPRESS_OUTPUT_DIR = '' #workaround for compressor bug #226
