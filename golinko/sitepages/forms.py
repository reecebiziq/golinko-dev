from django.forms import ModelForm
from django.core.mail import send_mail
from django import forms
from django.conf import settings

from .models import DesignOfficePage, SitePage,\
    SelectedClientsPage, CeePage, ContactPage, EssentialsPage

EMAIL_TO = 'info@golinkodesign.com'


class ContactForm(forms.Form):
    name = forms.CharField(label='name')
    email = forms.CharField(label='email')
    tel = forms.CharField(label='tel', required=False)
    message = forms.CharField(label='message', widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs.update({
                'placeholder': field.label,
            })

    def send_mail(self):
        recip = EMAIL_TO
        sender = settings.DEFAULT_FROM_EMAIL
        output = []
        for name in self.fields.keys():
            val = self.cleaned_data.get(name, None)
            if val:
                output.append(u'{0}: {1}'.format(name, val))
        message = u'\n'.join(output)
        send_mail("golinkodesign.com Contact Form", message, sender, [recip])


# Admin forms
error = "The object with such slug is already created"


class SitePageForm(ModelForm):
    class Meta:
        model = SitePage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            DesignOfficePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except DesignOfficePage.DoesNotExist:
            try:
                SelectedClientsPage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except SelectedClientsPage.DoesNotExist:
                try:
                    CeePage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except CeePage.DoesNotExist:
                    try:
                        ContactPage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except ContactPage.DoesNotExist:
                        try:
                            EssentialsPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except EssentialsPage.DoesNotExist:
                            return slug


class DesignOfficePageForm(ModelForm):
    class Meta:
        model = DesignOfficePage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            SitePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except SitePage.DoesNotExist:
            try:
                SelectedClientsPage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except SelectedClientsPage.DoesNotExist:
                try:
                    CeePage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except CeePage.DoesNotExist:
                    try:
                        ContactPage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except ContactPage.DoesNotExist:
                        try:
                            EssentialsPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except EssentialsPage.DoesNotExist:
                            return slug


class SelectedClientsPageForm(ModelForm):
    class Meta:
        model = SelectedClientsPage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            SitePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except SitePage.DoesNotExist:
            try:
                DesignOfficePage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except DesignOfficePage.DoesNotExist:
                try:
                    CeePage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except CeePage.DoesNotExist:
                    try:
                        ContactPage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except ContactPage.DoesNotExist:
                        try:
                            EssentialsPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except EssentialsPage.DoesNotExist:
                            return slug


class CeePageForm(ModelForm):
    class Meta:
        model = CeePage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            SitePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except SitePage.DoesNotExist:
            try:
                DesignOfficePage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except DesignOfficePage.DoesNotExist:
                try:
                    SelectedClientsPage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except SelectedClientsPage.DoesNotExist:
                    try:
                        ContactPage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except ContactPage.DoesNotExist:
                        try:
                            EssentialsPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except EssentialsPage.DoesNotExist:
                            return slug


class ContactPageForm(ModelForm):
    class Meta:
        model = ContactPage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            SitePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except SitePage.DoesNotExist:
            try:
                DesignOfficePage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except DesignOfficePage.DoesNotExist:
                try:
                    SelectedClientsPage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except SelectedClientsPage.DoesNotExist:
                    try:
                        CeePage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except CeePage.DoesNotExist:
                        try:
                            EssentialsPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except EssentialsPage.DoesNotExist:
                            return slug


class EssentialsPageForm(ModelForm):
    class Meta:
        model = EssentialsPage

    def clean_slug(self):
        slug = self.cleaned_data['slug']
        try:
            SitePage.objects.get(slug=slug)
            raise forms.ValidationError(error)
        except SitePage.DoesNotExist:
            try:
                DesignOfficePage.objects.get(slug=slug)
                raise forms.ValidationError(error)
            except DesignOfficePage.DoesNotExist:
                try:
                    SelectedClientsPage.objects.get(slug=slug)
                    raise forms.ValidationError(error)
                except SelectedClientsPage.DoesNotExist:
                    try:
                        CeePage.objects.get(slug=slug)
                        raise forms.ValidationError(error)
                    except CeePage.DoesNotExist:
                        try:
                            ContactPage.objects.get(slug=slug)
                            raise forms.ValidationError(error)
                        except ContactPage.DoesNotExist:
                            return slug
