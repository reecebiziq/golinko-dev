# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SitePage.content1_image'
        db.add_column('sitepages_sitepage', 'content1_image',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='content1_image', null=True, to=orm['media.Image']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SitePage.content1_image'
        db.delete_column('sitepages_sitepage', 'content1_image_id')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['media.MediaItem']},
            'alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'filename': ('cms.apps.media.fields.ImageField', [], {'max_length': '100'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'media.mediaitem': {
            'Meta': {'object_name': 'MediaItem'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_media.mediaitem_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'uuid': ('cms.apps.utils.fields.UUIDB64Field', [], {'default': "u'IyrHuNQnQyqU5O2H3Rppww'", 'unique': 'True', 'max_length': '22'})
        },
        'media.mediarelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'MediaRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mediarelation_set'", 'to': "orm['media.MediaItem']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'media.mediasubrelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'object_name': 'MediaSubRelation', 'db_table': "'media_mediarelation'", '_ormbases': ['media.MediaRelation'], 'proxy': 'True'}
        },
        'sitepages.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'menu_image_active': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'menu_image_hover': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sitepages.essential': {
            'Meta': {'ordering': "['order']", 'object_name': 'Essential'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sitepages.sitepage': {
            'Meta': {'object_name': 'SitePage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sitepages.Category']"}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content2_text': ('django.db.models.fields.TextField', [], {}),
            'content2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'related_images': ('cms.apps.media.fields.related.MediaGenericRelation', [], {'to': "orm['media.MediaSubRelation']"}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['sitepages']