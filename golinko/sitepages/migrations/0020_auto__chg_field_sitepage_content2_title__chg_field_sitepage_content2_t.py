# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SitePage.content2_title'
        db.alter_column('sitepages_sitepage', 'content2_title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'SitePage.content2_text'
        db.alter_column('sitepages_sitepage', 'content2_text', self.gf('django.db.models.fields.TextField')(null=True))
        # Adding field 'EssentialsPage.menu_image_default'
        db.add_column('sitepages_essentialspage', 'menu_image_default',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='essentials_menu_image_default', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'EssentialsPage.menu_image_hover'
        db.add_column('sitepages_essentialspage', 'menu_image_hover',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='essentials_menu_image_hover', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'EssentialsPage.menu_image_active'
        db.add_column('sitepages_essentialspage', 'menu_image_active',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='essentials_menu_image_active', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'DesignOfficePage.menu_image_default'
        db.add_column('sitepages_designofficepage', 'menu_image_default',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='design_office_menu_image_default', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'DesignOfficePage.menu_image_hover'
        db.add_column('sitepages_designofficepage', 'menu_image_hover',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='design_office_menu_image_hover', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'DesignOfficePage.menu_image_active'
        db.add_column('sitepages_designofficepage', 'menu_image_active',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='design_office_menu_image_active', null=True, to=orm['media.Image']),
                      keep_default=False)


        # Changing field 'DesignOfficePage.content1_title'
        db.alter_column('sitepages_designofficepage', 'content1_title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))
        # Adding field 'SelectedClientsPage.menu_image_default'
        db.add_column('sitepages_selectedclientspage', 'menu_image_default',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='selected_clients_menu_image_default', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'SelectedClientsPage.menu_image_hover'
        db.add_column('sitepages_selectedclientspage', 'menu_image_hover',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='selected_clients_menu_image_hover', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'SelectedClientsPage.menu_image_active'
        db.add_column('sitepages_selectedclientspage', 'menu_image_active',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='selected_clients_menu_image_active', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'CeePage.menu_image_default'
        db.add_column('sitepages_ceepage', 'menu_image_default',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cee_menu_image_default', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'CeePage.menu_image_hover'
        db.add_column('sitepages_ceepage', 'menu_image_hover',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cee_menu_image_hover', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'CeePage.menu_image_active'
        db.add_column('sitepages_ceepage', 'menu_image_active',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cee_menu_image_active', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'ContactPage.refresh_image_default'
        db.add_column('sitepages_contactpage', 'refresh_image_default',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='refresh_image_default', null=True, to=orm['media.Image']),
                      keep_default=False)

        # Adding field 'ContactPage.refresh_image_hover'
        db.add_column('sitepages_contactpage', 'refresh_image_hover',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='refresh_image_hover', null=True, to=orm['media.Image']),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SitePage.content2_title'
        raise RuntimeError("Cannot reverse this migration. 'SitePage.content2_title' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'SitePage.content2_text'
        raise RuntimeError("Cannot reverse this migration. 'SitePage.content2_text' and its values cannot be restored.")
        # Deleting field 'EssentialsPage.menu_image_default'
        db.delete_column('sitepages_essentialspage', 'menu_image_default_id')

        # Deleting field 'EssentialsPage.menu_image_hover'
        db.delete_column('sitepages_essentialspage', 'menu_image_hover_id')

        # Deleting field 'EssentialsPage.menu_image_active'
        db.delete_column('sitepages_essentialspage', 'menu_image_active_id')

        # Deleting field 'DesignOfficePage.menu_image_default'
        db.delete_column('sitepages_designofficepage', 'menu_image_default_id')

        # Deleting field 'DesignOfficePage.menu_image_hover'
        db.delete_column('sitepages_designofficepage', 'menu_image_hover_id')

        # Deleting field 'DesignOfficePage.menu_image_active'
        db.delete_column('sitepages_designofficepage', 'menu_image_active_id')


        # User chose to not deal with backwards NULL issues for 'DesignOfficePage.content1_title'
        raise RuntimeError("Cannot reverse this migration. 'DesignOfficePage.content1_title' and its values cannot be restored.")
        # Deleting field 'SelectedClientsPage.menu_image_default'
        db.delete_column('sitepages_selectedclientspage', 'menu_image_default_id')

        # Deleting field 'SelectedClientsPage.menu_image_hover'
        db.delete_column('sitepages_selectedclientspage', 'menu_image_hover_id')

        # Deleting field 'SelectedClientsPage.menu_image_active'
        db.delete_column('sitepages_selectedclientspage', 'menu_image_active_id')

        # Deleting field 'CeePage.menu_image_default'
        db.delete_column('sitepages_ceepage', 'menu_image_default_id')

        # Deleting field 'CeePage.menu_image_hover'
        db.delete_column('sitepages_ceepage', 'menu_image_hover_id')

        # Deleting field 'CeePage.menu_image_active'
        db.delete_column('sitepages_ceepage', 'menu_image_active_id')

        # Deleting field 'ContactPage.refresh_image_default'
        db.delete_column('sitepages_contactpage', 'refresh_image_default_id')

        # Deleting field 'ContactPage.refresh_image_hover'
        db.delete_column('sitepages_contactpage', 'refresh_image_hover_id')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'landingpages.passthroughpage': {
            'Meta': {'object_name': 'PassthroughPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'media.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['media.MediaItem']},
            'alt_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'filename': ('cms.apps.media.fields.ImageField', [], {'max_length': '100'}),
            'height': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'mediaitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['media.MediaItem']", 'unique': 'True', 'primary_key': 'True'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'media.mediaitem': {
            'Meta': {'object_name': 'MediaItem'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_media.mediaitem_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'uuid': ('cms.apps.utils.fields.UUIDB64Field', [], {'default': "u'CapDXH6sSSOZLhx6FttDFQ'", 'unique': 'True', 'max_length': '22'})
        },
        'media.mediarelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'unique_together': "(('item', 'object_id', 'content_type'),)", 'object_name': 'MediaRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'mediarelation_set'", 'to': "orm['media.MediaItem']"}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'sort': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        'media.mediasubrelation': {
            'Meta': {'ordering': "('content_type', 'sort')", 'object_name': 'MediaSubRelation', 'db_table': "'media_mediarelation'", '_ormbases': ['media.MediaRelation'], 'proxy': 'True'}
        },
        'menus.category': {
            'Meta': {'object_name': 'Category'},
            'background_image': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'background_image'", 'to': "orm['media.Image']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_active'", 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_default'", 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menu_image_hover'", 'to': "orm['media.Image']"}),
            'passthrough_page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['landingpages.PassthroughPage']"}),
            'show_on_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show_on_pages': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sitepages.ceepage': {
            'Meta': {'object_name': 'CeePage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cee_category'", 'to': "orm['menus.Category']"}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cee_content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cee_content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cee_menu_image_active'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cee_menu_image_default'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cee_menu_image_hover'", 'null': 'True', 'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sitepages.contactpage': {
            'Meta': {'object_name': 'ContactPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contact_category'", 'to': "orm['menus.Category']"}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'contact_content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content2_text': ('django.db.models.fields.TextField', [], {}),
            'content2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'refresh_image_default': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'refresh_image_default'", 'null': 'True', 'to': "orm['media.Image']"}),
            'refresh_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'refresh_image_hover'", 'null': 'True', 'to': "orm['media.Image']"}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sitepages.designofficepage': {
            'Meta': {'object_name': 'DesignOfficePage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'design_office_category'", 'to': "orm['menus.Category']"}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'design_office_content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'content2_text': ('django.db.models.fields.TextField', [], {}),
            'content2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'design_office_content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'design_office_menu_image_active'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'design_office_menu_image_default'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'design_office_menu_image_hover'", 'null': 'True', 'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sitepages.essential': {
            'Meta': {'ordering': "['order']", 'object_name': 'Essential'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'sitepages.essentialspage': {
            'Meta': {'object_name': 'EssentialsPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'essentials_category'", 'to': "orm['menus.Category']"}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'essentials_content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential10_text': ('django.db.models.fields.TextField', [], {}),
            'essential10_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential11_text': ('django.db.models.fields.TextField', [], {}),
            'essential11_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential12_text': ('django.db.models.fields.TextField', [], {}),
            'essential12_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential1_text': ('django.db.models.fields.TextField', [], {}),
            'essential1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential2_text': ('django.db.models.fields.TextField', [], {}),
            'essential2_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential3_text': ('django.db.models.fields.TextField', [], {}),
            'essential3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential4_text': ('django.db.models.fields.TextField', [], {}),
            'essential4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential5_text': ('django.db.models.fields.TextField', [], {}),
            'essential5_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential6_text': ('django.db.models.fields.TextField', [], {}),
            'essential6_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential7_text': ('django.db.models.fields.TextField', [], {}),
            'essential7_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential8_text': ('django.db.models.fields.TextField', [], {}),
            'essential8_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'essential9_text': ('django.db.models.fields.TextField', [], {}),
            'essential9_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'essentials_menu_image_active'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'essentials_menu_image_default'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'essentials_menu_image_hover'", 'null': 'True', 'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sitepages.selectedclientspage': {
            'Meta': {'object_name': 'SelectedClientsPage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'selected_clients_category'", 'to': "orm['menus.Category']"}),
            'col1_text': ('django.db.models.fields.TextField', [], {}),
            'col2_text': ('django.db.models.fields.TextField', [], {}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selected_clients_content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selected_clients_content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_image_active': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selected_clients_menu_image_active'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_default': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selected_clients_menu_image_default'", 'null': 'True', 'to': "orm['media.Image']"}),
            'menu_image_hover': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'selected_clients_menu_image_hover'", 'null': 'True', 'to': "orm['media.Image']"}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'sitepages.sitepage': {
            'Meta': {'object_name': 'SitePage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menus.Category']"}),
            'content1_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'content1_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content1_text': ('django.db.models.fields.TextField', [], {}),
            'content1_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content2_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'content2_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'content3_image': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'content3_image'", 'null': 'True', 'to': "orm['media.Image']"}),
            'content3_text': ('django.db.models.fields.TextField', [], {}),
            'content3_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'content4_text': ('django.db.models.fields.TextField', [], {}),
            'content4_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'headline': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'related_images': ('cms.apps.media.fields.related.MediaGenericRelation', [], {'to': "orm['media.MediaSubRelation']"}),
            'show_in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'sitemap_title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('cms.apps.utils.fields.AutoSlugField', [], {'prepopulate_from': "'title'", 'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'taggit.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100'})
        },
        'taggit.taggeditem': {
            'Meta': {'object_name': 'TaggedItem'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_tagged_items'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taggit_taggeditem_items'", 'to': "orm['taggit.Tag']"})
        }
    }

    complete_apps = ['sitepages']