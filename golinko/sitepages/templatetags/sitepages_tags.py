from django import template

from golinko.sidebars.models import Testimonial


register = template.Library()


@register.inclusion_tag('sitepages/my_client_say.html')
def my_client_say_tag(number=0):
    try:
        testimonial = Testimonial.objects.filter(published=True)\
                                         .all().order_by('order')[0]
    except IndexError:
        testimonial = None
    return {'testimonial': testimonial, 'number': number}
