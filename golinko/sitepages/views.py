from django import http
from django.views.generic import DetailView
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext

from .models import SitePage, Essential, DesignOfficePage,\
    SelectedClientsPage, CeePage, ContactPage, EssentialsPage, Category
from golinko.sidebars.models import Award, Testimonial

from annoying.decorators import ajax_request
from .forms import ContactForm


def site_page(request, slug):
    try:
        page = SitePage.objects.get(slug=slug)
    except SitePage.DoesNotExist:
        try:
            page = DesignOfficePage.objects.get(slug=slug)
        except DesignOfficePage.DoesNotExist:
            try:
                page = SelectedClientsPage.objects.get(slug=slug)
            except SelectedClientsPage.DoesNotExist:
                try:
                    page = CeePage.objects.get(slug=slug)
                except CeePage.DoesNotExist:
                    try:
                        page = ContactPage.objects.get(slug=slug)
                    except ContactPage.DoesNotExist:
                        try:
                            page = EssentialsPage.objects.get(slug=slug)
                        except EssentialsPage.DoesNotExist:
                            raise http.Http404()
    try:
        awards = Award.objects.filter(published=True)
    except Award.DoesNotExist:
        awards = None

    # contact form
    if getattr(page, 'show_contact_form', False):
        contact_form = ContactForm(request.POST or None)
        if request.method == 'POST':
            if contact_form.is_valid():
                contact_form.send_mail()
                return http.HttpResponseRedirect(
                        '{0}#contact-form'.format(request.path))
    else:
        contact_form = None

    return render_to_response('sitepages/site.html', {
        'page': page,
        'awards': awards,
        'contact_form': contact_form,
        }, context_instance=RequestContext(request))


class CategoryView(DetailView):
    queryset = Category.objects.all()
    template_name = 'sitepages/pass_through.html'
    context_object_name = 'category'

category = CategoryView.as_view()


class PassthroughTextView(DetailView):
    queryset = Category.objects.all()
    template_name = 'sitepages/pass_through_text.html'
    context_object_name = 'category'


passthrough_text = PassthroughTextView.as_view()


@ajax_request
def my_client_say(request):
    num = int(request.GET.get('testimonial_num', 0))
    tesimonials = Testimonial.objects.filter(published=True)\
                             .all().order_by('order')

    if num >= tesimonials.count():
        num = 0

    try:
        testimonial = tesimonials[num]
    except IndexError:
        raise http.Http404("%s - its wrong testimonial number" % num)

    text = render_to_string("sitepages/my_client_say.html",
                            {'testimonial': testimonial})
    return {'html': text, 'next_num': num + 1}


def index(request):
    return render(request, 'index.html')


@ajax_request
def submenu_items(request):
    slug = request.GET.get('slug', None)
    page = get_object_or_404(SitePage, slug=slug)

    text = render_to_string("sitepages/sitepages_parts/content.html",
                            {'page': page})
    images = render_to_string("sitepages/sitepages_parts/gallery.html",
                              {'page': page})
    secondary_content = render_to_string(
        "sitepages/sitepages_parts/secondary_content.html", {'page': page})
    return {'html': text, 'images': images,
            'secondary_content': secondary_content, 'title': page.title}
