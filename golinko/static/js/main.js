(function($) {
    /*
     * Prevents widowed words by adding &nbsp; between the last two words.
     */
    $.fn.unwidow = function() {
        return this.each(function() {
            var wordArray = $.trim($(this).html()).split(" ");
            if (wordArray.length > 2) {
                wordArray[wordArray.length-2] += "&nbsp;" + wordArray[wordArray.length-1];
                wordArray.pop();
                $(this).html(wordArray.join(" "));
            }
        });
    };
})(jQuery);

(function($) {
    $(function() {
        $(window).load(function() {
            $('h1,h2,h3,h4,h5').unwidow();
        });

        $('#footer').css('cursor', 'pointer').click(function() {
            window.location.href = '/to-portfolio/'
        });
    });
})(jQuery);

(function($) {
    $(function() {
        $(window).load(function() {
            $(".main-menu li").hover(
                function(){
                    var img = $(this).find('img')
                    img.attr('src', img.data('hover'))
                },
                function(){
                    var img = $(this).find('img')
                    img.attr('src', img.data('normal'))
                }
            )
        });
    });
})(jQuery);
