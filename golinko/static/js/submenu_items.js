 (function($){


     $.submenu_items = function(my_url){

         $(".class_submenu_items").live('click', function(e){
             e.preventDefault();
             var slug = $(this).data('slug');


             $.get(my_url, { slug: slug},
                 function(html){

                     $("#content_changes").html(html.html);
                     $("#image-gallery").html(html.images);
                     $("#secondary_content").html(html.secondary_content);

                     document.title = html.title;
                 });

             var pageurl = $(this).attr('href');

             if(pageurl!=window.location){
                 var History = window.History;
                 History.pushState('', '', pageurl);
             }

             $('.submenu-block .active').attr('class', 'submenu-item noactive');
             $(this).parent().attr('class', 'submenu-item active hovered');

             return false;
         });
     };
})(jQuery);
